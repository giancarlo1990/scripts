import math
import numpy as np
import matplotlib.pyplot as plt
def f(x):
    return math.sin(x)
x=np.array(range(200))*0.1
n=len(x)
y=np.zeros(n)

for i in range(0,n-1):
    y[i] = f(x[i])

    #plt.ion()
plt.plot(x,y)
plt.show()
