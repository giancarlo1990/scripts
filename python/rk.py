import numpy as np
import matplotlib.pyplot as plt

def f(x,y):
    return x**2-3*y
a = 0.0
b = 5
h= 0.01
def rk4(f,a,b,y0,h):
x = np.arange(a,b+h,h)
n = len(x)
y = np.zeros(n)
y[0] = 1
for i in range (0,n-1):
    k1 = f(x[i],y[i])
    k2 = f(x[i]+h/2,y[i]+k1*h/2)    
    k3 = f(x[i]+h/2,y[i]+k2*h/2)
    k4 = f(x[i]+h,y[i]+k3*h)
    y[i+1] = y[i]+(h/6)*(k1+2*k2+2*k3+k4)   
      #  print (x)
plt.plot (x,y)
plt.show()
