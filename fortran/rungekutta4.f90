program runge_kutta_4

integer::n 
real(4)h,x(1000),y(1000),x0,xf,y0 ,k1,k2,k3,k4
!real(4),external::f(1000)

read(*,*) x0,h,n,y0


x(1)=x0

y(1)=y0

do i=1,n+1

x(i+1)=x(i)+h

k1=h*f(x(i),y(i))

k2=h*f(x(i)+(0.5)*h,y(i)+(0.5)*k1)

k3=h*f(x(i)+(0.5)*h,y(i)+(0.5)*k2)

k4=h*f(x(i)+h,y(i)+k3)

y(i+1)=y(i)+(0.1667)*(k1+2*k2+2*k3+k4)

end do


do j=1,n+1

print*, x(j),y(j) 

end do

!print*,h

end program

function f(x,y)

!real(4)x,y

f=(y+1)*(x+1)*cos((x**2)+2*x)

return
end function
