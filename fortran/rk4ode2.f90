program runge_kutta4_ODE2

integer::n 
real(4)h,x(1000),y(1000),z(1000),x0,xf,y0,k1,k2,k3,k4,l1,l2,l3,l4
!real(4),external::f(1000)

read(*,*) xf,n


x(1)=0

y(1)=1
z(1)=0
y(n+1)=0
h=xf/n
do i=1,n

x(i+1)=x(i)+h

l1=h*z(i)
k1=h*f(x(i),y(i),z(i))
l2=h*(z(i)+(0.5)*k1)
k2=h*f(x(i)+(0.5)*h,y(i)+(0.5)*l1,z(i)+(0.5)*k1)
l3=h*(z(i)+(0.5)*k2)
k3=h*f(x(i)+(0.5)*h,y(i)+(0.5)*l2,z(i)+(0.5)*k2)
l4=h*(z(i)+k3)
k4=h*f(x(i)+h,y(i)+l3,z(i)+k3)

z(i+1)=z(i)+(0.1667)*(k1+2*k2+2*k3+k4)
y(i+1)=y(i)+(0.1667)*(l1+2*l2+2*l3+l4)

 
end do


do j=1,n+1

print*, x(j),y(j) 

end do

!print*,h

end program

function f(x,y,z)

!real(4)x,y

f=0.5*cos(0.1*x)-0.7*z+y-y**3

return
end function



