\babel@toc {spanish}{}
\beamer@sectionintoc {1}{Introducción}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Introducción}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Objetivos}{8}{0}{1}
\beamer@subsectionintoc {1}{3}{Antecedentes}{12}{0}{1}
\beamer@sectionintoc {2}{Oscilador de Duffing}{16}{0}{2}
\beamer@sectionintoc {3}{Detección de señal débil utilizando el oscilador de Duffing}{20}{0}{3}
\beamer@subsectionintoc {3}{1}{Detección de señal débil utilizando el oscilador de Duffing}{21}{0}{3}
\beamer@subsectionintoc {3}{2}{Calculo del umbral de cambio de estado}{26}{0}{3}
\beamer@subsectionintoc {3}{3}{Implementación de la señal de entrada y ruido en la ecuación de Duffing}{40}{0}{3}
\beamer@sectionintoc {4}{Doble acoplamiento del oscilador de Duffing}{49}{0}{4}
\beamer@sectionintoc {5}{Conclusión}{53}{0}{5}
\beamer@sectionintoc {6}{Bibliografía}{57}{0}{6}
