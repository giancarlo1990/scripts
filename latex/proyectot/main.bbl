\begin{thebibliography}{}

\bibitem[Jothimurugan et~al., 2015]{Jothimurugan_2015}
Jothimurugan, R., Thamilmaran, K., Rajasekar, S., y Sanjuán, M. A.~F. (2015).
\newblock Multiple resonance and anti-resonance in coupled duffing oscillators.
\newblock {\em Nonlinear Dynamics}, 83(4):1803--1814.

\bibitem[Jürgen et~al., 2008]{Duf}
Jürgen, H., KorschHans-Jörg, y Hartmann, J. (2008).
\newblock {\em The Duffing Oscillator}, pages 157--184.
\newblock Springer Berlin Heidelberg.

\bibitem[Li y Shen, 2009]{Li_2009}
Li, J. y Shen, Y. (2009).
\newblock The study of weak signal detection using duffing oscillators array.
\newblock In {\em 2009 {IEEE} Circuits and Systems International Conference on
  Testing and Diagnosis}. {IEEE}.

\bibitem[Liu y Wang, 2019]{Liu_2019}
Liu, X. y Wang, M. (2019).
\newblock A new type of duffing oscillator for weak signal detection.
\newblock In {\em Proceedings of the 2nd International Conference on
  Information Technologies and Electrical Engineering}. {ACM}.

\bibitem[Tian-liang, 2008]{Tian_Liang_2008}
Tian-liang, L. (2008).
\newblock Frequency estimation for weak signals based on chaos theory.
\newblock In {\em 2008 International Seminar on Future {BioMedical} Information
  Engineering}. {IEEE}.

\bibitem[Wang et~al., 2020a]{Wang2020}
Wang, K., Yan, X., Yang, Q., Hao, X., y Wang, J. (2020a).
\newblock Weak signal detection based on strongly coupled duffing-van der pol
  oscillator and long short-term memory.
\newblock {\em Journal of the Physical Society of Japan}, 89(1):014003.

\bibitem[Wang et~al., 2020b]{Wang_2020}
Wang, Q., Yang, Y., y Zhang, X. (2020b).
\newblock Weak signal detection based on mathieu-duffing oscillator with
  time-delay feedback and multiplicative noise.
\newblock {\em Chaos, Solitons {\&} Fractals}, 137:109832.

\bibitem[Yang et~al., 2019]{Yang_2019}
Yang, Y., Chen, J., Gao, Y., Fan, H., Yang, Y., Chen, J., Gao, Y., y Fan, H.
  (2019).
\newblock Detection of weak signal-to-noise ratio signal while drilling based
  on duffing chaotic oscillator.
\newblock In {\em 2019 6th International Conference on Information Science and
  Control Engineering ({ICISCE})}. {IEEE}.

\end{thebibliography}
