\babel@toc {spanish}{}
\contentsline {chapter}{INTRODUCCIÓN}{6}{chapter*.4}%
\contentsline {chapter}{\numberline {1}PLANTEAMIENTO DEL PROBLEMA}{7}{chapter.1}%
\contentsline {section}{\numberline {1.1}Descripción de la realidad problemática}{7}{section.1.1}%
\contentsline {section}{\numberline {1.2}Formulación del problema}{7}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Problema general}{7}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Problemas específicos}{8}{subsection.1.2.2}%
\contentsline {section}{\numberline {1.3}Objetivos}{8}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Objetivo general}{8}{subsection.1.3.1}%
\contentsline {subsection}{\numberline {1.3.2}Objetivos específicos}{8}{subsection.1.3.2}%
\contentsline {section}{\numberline {1.4}Justificación}{8}{section.1.4}%
\contentsline {section}{\numberline {1.5}Limitantes de la investigación}{9}{section.1.5}%
\contentsline {subsection}{\numberline {1.5.1}Teórica}{9}{subsection.1.5.1}%
\contentsline {subsection}{\numberline {1.5.2}Temporal}{9}{subsection.1.5.2}%
\contentsline {subsection}{\numberline {1.5.3}Espacial}{9}{subsection.1.5.3}%
\contentsline {chapter}{\numberline {2}MARCO TEÓRICO}{10}{chapter.2}%
\contentsline {section}{\numberline {2.1}Antecedente}{10}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Internacional}{10}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Nacional}{12}{subsection.2.1.2}%
\contentsline {section}{\numberline {2.2}Bases teóricas}{12}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Oscilador de Duffing}{12}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Teoría de detección de señales débiles}{13}{subsection.2.2.2}%
\contentsline {section}{\numberline {2.3}Conceptual}{14}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Oscilador de Duffing para la detección de señales débiles}{14}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Doble acoplamiento del oscilador de Duffing}{20}{subsection.2.3.2}%
\contentsline {section}{\numberline {2.4}Definición de términos básicos}{21}{section.2.4}%
\contentsline {chapter}{\numberline {3}HIPÓTESIS Y VARIABLES}{23}{chapter.3}%
\contentsline {section}{\numberline {3.1}Hipótesis}{23}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Hipótesis General}{23}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Hipótesis Especifica}{23}{subsection.3.1.2}%
\contentsline {section}{\numberline {3.2}Definición conceptual de variables}{23}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Variable dependiente}{23}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Variables independientes}{23}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {3.2.3}Operacionalización de variables}{23}{subsection.3.2.3}%
\contentsline {chapter}{\numberline {4}DISEÑO METODOLÓGICO}{25}{chapter.4}%
\contentsline {section}{\numberline {4.1}Tipo y diseño de investigación}{25}{section.4.1}%
\contentsline {section}{\numberline {4.2}Método de investigación}{25}{section.4.2}%
\contentsline {section}{\numberline {4.3}Población y muestra}{25}{section.4.3}%
\contentsline {section}{\numberline {4.4}Lugar de estudio}{25}{section.4.4}%
\contentsline {section}{\numberline {4.5}Técnica e instrumentos para la recolección de la información}{26}{section.4.5}%
\contentsline {subsection}{\numberline {4.5.1}Quite Universal Circuit Simulator (Qucs-S)}{26}{subsection.4.5.1}%
\contentsline {section}{\numberline {4.6}Análisis y procesamiento de datos}{26}{section.4.6}%
\contentsline {chapter}{\numberline {5}CRONOGRAMA DE ACTIVIDADES}{27}{chapter.5}%
\contentsline {chapter}{\numberline {6}PRESUPUESTO}{28}{chapter.6}%
\contentsline {chapter}{8. REFERENCIAS BIBLIOGRÁFICAS}{29}{section*.5}%
\contentsline {chapter}{\numberline {7}ANEXOS}{30}{chapter.7}%
\contentsline {section}{Matriz de consistencia}{30}{section*.7}%
