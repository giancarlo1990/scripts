function calor1D

clc,clear all, close all

alfa=1; dx=0.25; L=1; tmax=1;
dt=0.02;
CFL=0.16;

t=0:dt:tmax;
X=0:dx:L;
Nt=ceil(tmax/dt)+1;
Nx=length(X);
T=zeros(Nt,Nx);
T(1,:)=20;T(:,1)=100; T(:,end)=100;
T(1,1)=(T(1,2)+T(2,1))/2; T(1,end)=(T(1,end-1)+T(2,end))/2;

for f=2:Nt
  for c=2:Nx-1
  T(f,c)=T(f-1,c)+CFL*(T(f-1,c+1)-2*T(f-1,c)+T(f-1,c-1));
  end
end
T
%plot(X,T), grid on

figure
surf(X,t,T)
title('ECUACION DE CALOR'); xlabel('eje x');
ylabel('tiempo t'); zlabel('temperatura T');
end
