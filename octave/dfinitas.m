
clear;
N=100; 
zeta=0.01; 
M=300; 
% t= 0;t= 0.5;t= 1;t= 2 andt= 3, 
%
g=1; % gravedad
delta=10/N;
u(:,:) = zeros(N+1,M); 
h(:,:) = zeros(N+1,M); 
x=-5 : delta : 5; 
%x=0 : delta : 100;
u(1,:) = 0; u(N+1,:)= 0; 
%u(1,:) = 0; u(N+1,:)= 0;
u(:,1) = 0; %
h(1,:) = 1; h(N+1,:) = 1; 
%h(1,:) = 0; h(N+1,:) = 0;

for k=2:N
%h(k,1)=50-(45)*tanh((x(k)-7)*0.125); %Initial conditions
h(k,1)=1+(0.4)*exp(-5*(x(k))^2);
end

for j=2:M
A(1,N)=2*delta;
A(1,N+1)=zeta*u(2,j-1);
A(1,2)=zeta*h(2,j-1);

%fori= 1 
b(1,1)=2*delta*h(2,j-1)+zeta*u(2,j-1)*h(1,j)+zeta*h(2,j-1)*u(1,j);
A(2,1)=2*delta*h(2,j-1);
A(2,2)=zeta*u(2,j-1)*h(2,j-1);
%for i=1
A(2,N+1)=zeta*g*h(2,j-1);
b(2,1)=2*delta*h(2,j-1)*u(2,j-1)+zeta*u(2,j-1)*h(2,j-1)*u(1,j)+zeta*g*h(2,j-1)*h(1,j);
A(2*N-3,2*N-2) = 2*delta;
A(2*N-3,N-2 +N-1) =-zeta*u(N,j-1);
A(2*N-3,N-1-1) =-zeta*h(N,j-1);
%fori= 2 
for i=3:N-1
b(2*i-3,1) = 2*delta*h(i,j-1);
A(2*i-2,i-1) = 2*delta*h(i,j-1);
A(2*i-2,i+ 1-1) =zeta*u(i,j-1)*h(i,j-1);
A(2*i-2,i-1-1) =-zeta*u(i,j-1)*h(i,j-1);
A(2*i-2,N-2+i+ 1)=zeta*g*h(i,j-1);
A(2*i-2,N-2+i-1)=-zeta*g*h(i,j-1);
%for i= 2 
b(2*i-2,1)=2*delta*h(i,j-1)*u(i,j-1); 
%for i=3 
b(2*N-3,1) = 2*delta*h(N,j-1)-zeta*u(N,j-1)*h(N+ 1,j)-zeta*h(N,j-1)*u(N+ 1,j);A(2*N-2,N-1)=2*delta*h(N,j-1);
A(2*N-2,N-1-1) =-zeta*u(N,j-1)*h(N,j-1);
A(2*N-2,N-2 +N-1) =-zeta*g*h(N,j-1);
%for i=3 
b(2*N-2,1) = 2*delta*h(N,j-1)*u(N,j-1)-zeta*u(N,j-1)*h(N,j-1)*u(N+ 1,j)-zeta*g*h(N,j-1)*h(N+ 1,j);
A(2*i-3,N-2 +i) = 2*delta;
A(2*i-3,N-2 +i+ 1) =zeta*u(i,j-1);
A(2*i-3,N-2 +i-1) =-zeta*u(i,j-1);
A(2*i-3,i+ 1-1) =zeta*h(i,j-1);
A(2*i-3,i-1-1) =-zeta*h(i,j-1);

end
% resolviendo 
y=A\b;

u(2:N,j)=y(1:N-1); h(2:N,j)=y(N:2*N-2);
end


figure 
subplot(1,2,1); plot(x, h(:, 1), 'b-' ); 
title('h at t=0'); 
axis([-5 5 0.5 1.5]); 
subplot(1,2,2); plot(x,h(:,1).*u(:,1),'r-'); 
title('h*u at t=0'); 
axis([-5 5 -0.5 0.5]);

figure 
subplot(1,2,1); plot(x, h(:, 25), 'b-' ); 
title('h at t=0.25'); 
axis([-5 5 0.5 1.5]); 
subplot(1,2,2); plot(x,h(:,25).*u(:,25),'r-'); 
title('h*u at t=0.25'); 
axis([-5 5 -0.5 0.5]);

figure  
subplot(1,2,1); plot(x, h(:,50), 'b-'); 
title('h at t=0.5'); 
axis([-5 5 0.5 1.5]); 
subplot(1,2,2); plot(x,h(:,50).*u(:,50),'r-'); 
title('h*u at t=0.5'); 
axis([-5 5 -0.5 0.5]);

figure 
subplot(1,2,1); plot(x,h(:,100),'b-'); 
title('h at t=1'); 
axis([-5 5 0.5 1.5]); 
subplot(1,2,2); plot(x,h(:,100).*u(:,100),'r-'); 
title('h*u at t=1'); 
axis([-5 5 -0.5 0.5]);


figure 
subplot(1,2,1); plot(x, h(:, 150), 'b-' ); 
title('h at t=1.5'); 
axis([-5 5 0.5 1.5]); 
subplot(1,2,2); plot(x,h(:,150).*u(:,150),'r-'); 
title('h*u at t=1.5'); 
axis([-5 5 -0.5 0.5]);

figure 
subplot(1,2,1); plot(x,h(:,200),'b-'); 
title('h at t=2'); 
axis([-5 5 0.5 1.5]); 
subplot(1,2,2); plot(x,h(:,200).*u(:,200),'r-'); 
title('h*u at t=2'); 
axis([-5 5 -0.5 0.5]);


figure 
subplot(1,2,1); plot(x,h(:,250),'b-'); 
title('h at t=2.5'); 
axis([-5 5 0.5 1.5]); 
subplot(1,2,2); plot(x,h(:,250).*u(:,250),'r-'); 
title('h*u at t=2.5'); 
axis([-5 5 -0.5 0.5]);


figure 
subplot(1,2,1); plot(x,h(:,300),'b-'); 
title('h at t=3'); 
axis([-5 5 0.5 1.5]); 
subplot(1,2,2); plot(x,h(:,300).*u(:,300),'r-'); 
title('h*u at t=3'); 
axis([-5 5 -0.5 0.5]);



